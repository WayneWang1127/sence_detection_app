package com.compal.sdbg.optical.ceisencedetection;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import org.tensorflow.lite.Interpreter;

public class CeiSceneDetectModelLite {
    private final String TAG = "CeiSenceDetectionModel";
    private final boolean DEBUG = true;

    private String MODEL_NAME = "ceiscenedetect_v1.0_2019.03.05.tflite";
    private final int INPUT_IMG_WIDTH = 224;
    private final int INPUT_IMG_HEIGHT = 224;
    private final int OUTPUT_CLASSES = 13;
    private float confidence_threshold = 0.8f;

    /** AssetManager */
    private AssetManager mAssetManager;

    /** Class names */
    private CeiSceneDetectClasses mClassName;

    /** Store the bitmap after pre-process */
    private Bitmap mCookedBitmap;

    /** An instance of the driver class to run model inference with Tensorflow Lite. */
    private Interpreter tflite;

    /** Options for configuring the Interpreter. */
    private final Interpreter.Options tfliteOptions = new Interpreter.Options();

    /** The loaded TensorFlow Lite model. */
    private MappedByteBuffer tfliteModel;

    /** A ByteBuffer to hold image data, to be feed into Tensorflow Lite as inputs. */
    protected ByteBuffer imgData = null;

    /** Dimensions of inputs. */
    private static final int DIM_BATCH_SIZE = 1;
    private static final int DIM_PIXEL_SIZE = 3;

    /**
     * An array to hold inference results, to be feed into Tensorflow Lite as outputs. This isn't part
     * of the super class, because we need a primitive array here.
     */
    private float[][] labelProbArray;

    public CeiSceneDetectModelLite(AssetManager assetManager) {
        mAssetManager = assetManager;
        mClassName = new CeiSceneDetectClasses();
        labelProbArray = new float[1][OUTPUT_CLASSES];
        load_model(MODEL_NAME);
    }

    /**
     * Memory-map the model file in Assets.
     */
    private MappedByteBuffer loadModelFile(String model) throws IOException {
        AssetFileDescriptor fileDescriptor = mAssetManager.openFd(model);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();

        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    /** load infer model */
    private void load_model(String model) {
        try {
            tfliteModel = loadModelFile(model);
            tflite = new Interpreter(tfliteModel, tfliteOptions);
//            labelList = loadLabelList(activity);
            imgData =
                    ByteBuffer.allocateDirect(
                            DIM_BATCH_SIZE
                            * INPUT_IMG_WIDTH
                            * INPUT_IMG_HEIGHT
                            * DIM_PIXEL_SIZE
                            * 4);
            imgData.order(ByteOrder.nativeOrder());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void convertBitmapToByteBuffer(Bitmap bitmap) {
        int[] intValue = new int[INPUT_IMG_WIDTH * INPUT_IMG_HEIGHT];

        if (imgData == null) {
            return;
        }
        imgData.rewind();

        /** Create float array for TensorFlow feeding */
        bitmap.getPixels(
                intValue,
                0,
                bitmap.getWidth(),
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight()
        );

        /** Pixel format: [R1, G1, B1, R2, G2, B2, ... ] */
        for (int i = 0; i < intValue.length; i++) {
            final int val = intValue[i];
            imgData.putFloat(((val >> 16) & 0xFF) / 255.f);
            imgData.putFloat(((val >> 8) & 0xFF) / 255.f);
            imgData.putFloat((val & 0xFF) / 255.f);
        }
    }

    public void setInput(Bitmap bitmap) {
        /** Resize image to fit model's input shape */
        int orgWidth = bitmap.getWidth();
        int orgHeight = bitmap.getHeight();
        float scaleWidth = (float) INPUT_IMG_WIDTH / orgWidth;
        float scaleHeight = (float) INPUT_IMG_HEIGHT / orgHeight;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        mCookedBitmap = Bitmap.createBitmap(
                bitmap, 0, 0, orgWidth, orgHeight, matrix, true);

//        if (DEBUG) {
//            Log.i(TAG, "org image size (W: " + orgWidth + ", H: " + orgHeight + ")");
//            Log.i(TAG, "scale coefficient (sW: " + scaleWidth + ", sH: " + scaleHeight + ")");
//            Log.i(TAG, "scaled image size (W: " + INPUT_IMG_WIDTH + ", H: " + INPUT_IMG_HEIGHT + ")");
//        }

        convertBitmapToByteBuffer(mCookedBitmap);
    }

    public void run() {
        tflite.run(imgData, labelProbArray);
    }

    public String[] getPrediction() {
        String[] prediction = new String[255];

        int n = 0;
        float max_value = 0.0f;
        int max_index = 0;

        while (n < OUTPUT_CLASSES) {
            if (labelProbArray[0][n] > max_value) {
                max_value = labelProbArray[0][n];
                max_index = n;
            }
            n += 1;
        }

        if (max_value > confidence_threshold) {
            prediction[0] = String.valueOf(max_index);
            prediction[1] = mClassName.getClassName(max_index);
            prediction[2] = String.format("%.2f", max_value);
        } else {
            prediction[0] = "";
            prediction[1] = "";
            prediction[2] = "";
        }

        return prediction;
    }
}
