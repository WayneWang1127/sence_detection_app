package com.compal.sdbg.optical.ceisencedetection;

public class CeiSceneDetectClasses {
    private String[] mClasses;

    public CeiSceneDetectClasses() {
        super();

        /** Create classes table */
        String[] classes = {
                "cat_dog",
                "night_shot",
                "food",
                "sunset",
                "snow",
                "portrait",
                "grass",
                "blue_sky",
                "document",
                "flowers",
                "beach",
                "back_light",
                "others"
        };

        mClasses = classes;
    }

    public String getClassName(int index) {
        return mClasses[index];
    }
}
