/**
 * CEI Scene Detection Model
 *
 * Version: 1.0.0
 *
 * Pre-train model is using MobileNetV2
 */

package com.compal.sdbg.optical.ceisencedetection;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

public class CeiSceneDetectModel {
    private final String TAG = "CeiSenceDetectionModel";
    private final boolean DEBUG = true;

    /** Model settings */
    private final String MODEL_NAME = "ceiscenedetect.2019.2.21.pb";
    private final String INPUT_NAME = "input_1";
    private final String[] OUTPUT_NAME = {"output/Softmax"};
    private final int INPUT_IMG_WIDTH = 224;
    private final int INPUT_IMG_HEIGHT = 224;
    private final int OUTPUT_CLASSES = 13;

    private float confidence_threshold = 0.8f;

    /** Tensor Flow Interface */
    private TensorFlowInferenceInterface mTensorFlowInterface;

    /** Store the bitmap after pre-process */
    private Bitmap mCookedBitmap;

    /** Store the float array for model input */
    private float[] mCookedInputValue;

    /** Store the model output value after run */
    private float[] mOutputValue;

    /** Class names */
    private CeiSceneDetectClasses mClassName;

    public CeiSceneDetectModel(AssetManager assetManager) {
        super();

        mTensorFlowInterface = new TensorFlowInferenceInterface(assetManager, MODEL_NAME);
        mClassName = new CeiSceneDetectClasses();
    }

    private float[] convertBitmapToModelInput(Bitmap bitmap) {
        int[] intValue = new int[INPUT_IMG_WIDTH * INPUT_IMG_HEIGHT];
        float[] inputValue = new float[INPUT_IMG_WIDTH * INPUT_IMG_HEIGHT * 3];

        /** Create float array for TensorFlow feeding */
        bitmap.getPixels(
                intValue,
                0,
                bitmap.getWidth(),
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight()
        );

        /** Pixel format: [R1, G1, B1, R2, G2, B2, ... ] */
        // FIXME: maybe have batter method to convert Bitmap to Array?
        for (int i = 0; i < intValue.length; i++) {
            final int val = intValue[i];
            inputValue[i * 3] = (val >> 16) & 0xFF;
            inputValue[i * 3 + 1] = (val >> 8) & 0xFF;
            inputValue[i * 3 + 2] = (val) & 0xFF;

            /** Normalization */
            inputValue[i * 3] /= 255.0;
            inputValue[i * 3 + 1] /= 255.0;
            inputValue[i * 3 + 2] /= 255.0;
        }

        return inputValue;
    }

    public void setInput(Bitmap bitmap) {
        /** Resize image to fit model's input shape */
        int orgWidth = bitmap.getWidth();
        int orgHeight = bitmap.getHeight();
        float scaleWidth = (float) INPUT_IMG_WIDTH / orgWidth;
        float scaleHeight = (float) INPUT_IMG_HEIGHT / orgHeight;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        mCookedBitmap = Bitmap.createBitmap(
                bitmap, 0, 0, orgWidth, orgHeight, matrix, true);

        if (DEBUG) {
            Log.i(TAG, "org image size (W: " + orgWidth + ", H: " + orgHeight + ")");
            Log.i(TAG, "scale coefficient (sW: " + scaleWidth + ", sH: " + scaleHeight + ")");
            Log.i(TAG, "scaled image size (W: " + INPUT_IMG_WIDTH + ", H: " + INPUT_IMG_HEIGHT + ")");
        }

        mCookedInputValue = convertBitmapToModelInput(mCookedBitmap);
    }

    public void run() {
        /** Feed image into model */
        mTensorFlowInterface.feed(
                INPUT_NAME,
                mCookedInputValue,
                1, INPUT_IMG_WIDTH, INPUT_IMG_HEIGHT, 3
        );

        /** Run model output */
        mTensorFlowInterface.run(OUTPUT_NAME);

        /** Fetch output data in a float array */
        float[] outputs = new float[OUTPUT_CLASSES];  // Output size depends on model's design
        mTensorFlowInterface.fetch(OUTPUT_NAME[0], outputs);

        mOutputValue = outputs;

        if (DEBUG) {
            StringBuilder debug_output = new StringBuilder("{");
            for (int i = 0; i < OUTPUT_CLASSES; i++) {
                debug_output.append(String.format("%d: %.4f", i, outputs[i]));
                if (i != OUTPUT_CLASSES - 1) {
                    debug_output.append(", ");
                }
            }
            debug_output.append("}");
            Log.i(TAG, "output = " + debug_output.toString());
        }
    }

    public String[] getPrediction() {
        /**
         * Prediction format:
         * prediction[0]: classes number
         * prediction[1]: classes name
         * prediction[2]: probability
         */
        String[] prediction = new String[3];

        int n = 0;
        float max_value = 0.0f;
        int max_index = 0;

        while (n < OUTPUT_CLASSES) {
            if (mOutputValue[n] > max_value) {
                max_value = mOutputValue[n];
                max_index = n;
            }
            n += 1;
        }

        if (max_value > confidence_threshold) {
            prediction[0] = String.valueOf(max_index);
            prediction[1] = mClassName.getClassName(max_index);
            prediction[2] = String.format("%.2f", max_value);
        } else {
            prediction[0] = "";
            prediction[1] = "";
            prediction[2] = "";
        }

        return prediction;
    }

    public Bitmap getCookedBitmap() {
        return mCookedBitmap;
    }

    public float[] getCookedInputValue() {
        return mCookedInputValue;
    }

    public void setConfidenceThreshold(int threshold) {
        confidence_threshold = threshold;
    }
}
