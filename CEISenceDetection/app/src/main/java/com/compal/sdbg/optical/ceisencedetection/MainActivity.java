package com.compal.sdbg.optical.ceisencedetection;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private String TAG = "CeiSenceDetection";

    private Button mBtnCapture;
    private TextureView mTexturePreview;
    private TextView mSpeedTextView;
    private TextView mClassTextView;
    private ImageView mClassImageView;

    private static final int UPDATE_EXECUTE_SPEED = 1;
    private static final int UPDATE_CLASS_NAME = 2;
    private static final int UPDATE_CLASS_IMAGE = 3;

    // Check state orientation of output image
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;

    // Save to FILE
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    private Handler mPredictHandler;
    private HandlerThread mPredictThread;

    private Vibrator mVibrator;

//    private TensorFlowInferenceInterface mTensorFlowInterface;
//    private CeiSceneDetectModel mModel;
    private CeiSceneDetectModelLite mModel;

    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    /**
     * This thread is doing below things:
     *   1. Get preview screen from TextureView.
     *   2. Feed image into model class.
     *   3. Run model.
     *   4. Fetch output data from model.
     *   5. Send message to main thread to draw object box on UI.
     */
    private Runnable mDoPredict = new Runnable() {

        int count = 0;           // TODO
        int lastClassNum = -1;   // TODO
        final int CLASS_THD = 3; // TODO

        @SuppressLint("DefaultLocale")
        @Override
        public void run() {
            synchronized (this) {
                while (!Thread.currentThread().isInterrupted()) {

                    /** Avoid TextureView is not ready when thread is running */
                    if (!mTexturePreview.isAvailable()) {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }

                    /** Do prediction here */
                    long startTime = System.nanoTime();

                    /** Get preview image (Bitmap) from TextureView */
                    Bitmap bitmap = mTexturePreview.getBitmap();

                    /** Set input image to model */
                    mModel.setInput(bitmap);

                    /** Run model */
                    mModel.run();

                    long endTime = System.nanoTime();
                    double diffTime = (endTime - startTime) / 1e6;
                    double fps = (1.0f / diffTime) * 1e3;

                    /** Get model output */
                    String[] prediction;
                    prediction = mModel.getPrediction();

                    String class_output = String.format(
                            "Class: %s (%s)",
                            prediction[1],
                            prediction[2]
                    );

                    int classNum;
                    try {
                        classNum = Integer.valueOf(prediction[0]);
                    } catch (NumberFormatException e) {
                        classNum = -1;
                    }

                    // TODO
                    if (lastClassNum == classNum) {
                        if (count < CLASS_THD) {
                            count++;
                            classNum = -1;
                        }
                    } else {
                        lastClassNum = classNum;
                        count = 0;
                        classNum = -1;
                    }

                    /** Send message to main Thread to update UI */
                    if (!Thread.currentThread().isInterrupted()) {
                        /* Update class name */
                        Message classMsg = new Message();
                        Bundle classData = new Bundle();

                        classData.putString("class_name", class_output);
                        classMsg.what = UPDATE_CLASS_NAME;
                        classMsg.setData(classData);
                        mUpdateUI.sendMessage(classMsg);

                        /** Update class image */
                        Message classNumMsg = new Message();
                        Bundle classNumData = new Bundle();

                        classNumData.putInt("class_num", classNum);
                        classNumMsg.what = UPDATE_CLASS_IMAGE;
                        classNumMsg.setData(classNumData);
                        mUpdateUI.sendMessage(classNumMsg);

                        /** Update execute speed */
                        Message speedMsg = new Message();
                        Bundle speedData = new Bundle();

                        speedData.putString("execute_speed",
                                String.format("Execute: %.2f (ms) / FPS: %.2f",
                                        diffTime,
                                        fps));
                        speedMsg.what = UPDATE_EXECUTE_SPEED;
                        speedMsg.setData(speedData);
                        mUpdateUI.sendMessage(speedMsg);
                    } else {
                        Log.d(TAG, "Thread is interrupted");
                    }
                }
            }
        }
    };

    /*
     * Handle message on main thread (Update UI)
     */
    @SuppressLint("HandlerLeak")
    private Handler mUpdateUI = new Handler() {
        private boolean needVibrate = true;
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_EXECUTE_SPEED:
                    mSpeedTextView.setText(msg.getData().getString("execute_speed"));
                    break;
                case UPDATE_CLASS_NAME:
                    mClassTextView.setText(msg.getData().getString("class_name"));
                    break;
                case UPDATE_CLASS_IMAGE:
                    int class_num = msg.getData().getInt("class_num");

                    if (class_num >= 0 && class_num != 12) {
                        if (needVibrate) {
                            mVibrator.vibrate(500);
                            needVibrate = false;
                        }
                        mClassImageView.setImageLevel(class_num);
                        mClassImageView.setVisibility(View.VISIBLE);
                    } else {
                        needVibrate = true;
                        mClassImageView.setVisibility(View.GONE);
                    }
            }
        }
    };

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTexturePreview = findViewById(R.id.texturePreview);
        assert mTexturePreview != null;
        mTexturePreview.setSurfaceTextureListener(textureListen);

        mBtnCapture = findViewById(R.id.btnCapture);
        mBtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                takePicture(); TODO
            }
        });

        mSpeedTextView = findViewById(R.id.speedTextView);
        mSpeedTextView.setText("");
        mSpeedTextView.setBackgroundColor(Color.WHITE);
        mSpeedTextView.setTextSize(15.0f);

        mClassTextView = findViewById(R.id.classTextView);
        mClassTextView.setText("");
        mClassTextView.setBackgroundColor(Color.WHITE);
        mClassTextView.setTextSize(15.0f);

        mClassImageView = findViewById(R.id.classImageView);
        mClassImageView.setBackgroundColor(Color.argb(200, 150, 150, 150));
        mClassImageView.setVisibility(View.GONE);

        /** Get vibrator service */
        mVibrator = (Vibrator) getApplication().getSystemService(Service.VIBRATOR_SERVICE);

        /** Load TensorFlow pre-built model */
        AssetManager assetManager  = getAssets();

//        mModel = new CeiSceneDetectModel(assetManager);
        mModel = new CeiSceneDetectModelLite(assetManager);
    }

    private void takePicture() {
        if (cameraDevice == null)
            return;

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            assert manager != null;
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            jpegSizes = Objects.requireNonNull(characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP))
                    .getOutputSizes(ImageFormat.JPEG);

            // Capture image with custom size
            int width = 640;
            int height = 480;
            if (jpegSizes != null && jpegSizes.length > 0) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }

            final ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurface = new ArrayList<>(2);
            outputSurface.add(reader.getSurface());
            outputSurface.add(new Surface(mTexturePreview.getSurfaceTexture()));

            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            /** Check orientation base on device */
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            file = new File(Environment.getExternalStorageDirectory() + "/" + UUID.randomUUID().toString() + ".jpg");
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader imageReader) {
                    Image image = null;
                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (image != null) {
                            image.close();
                        }
                    }
                }
                private void save(byte[] bytes) throws IOException {
                    OutputStream outputStream = null;
                    try {
                        outputStream = new FileOutputStream(file);
                        outputStream.write(bytes);
                    } finally {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    }
                }
            };

            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    Toast.makeText(MainActivity.this, "Saved " + file, Toast.LENGTH_SHORT).show();
                    createCameraPreview();
                }
            };

            cameraDevice.createCaptureSession(outputSurface, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    try {
                        cameraCaptureSession.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.i(TAG, "Orientation: " + newConfig.orientation);
    }

    private void createCameraPreview() {
        try {

            /** Prepare Surface */
            SurfaceTexture texture = mTexturePreview.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);

            /** Create Capture Request */
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            /** Create Capture Session */
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(MainActivity.this, "Changed", Toast.LENGTH_SHORT).show();
                }
            }, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void updatePreview() {
        if (cameraDevice == null)
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();

        try {
            /** Set Repeating Request */
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /** Open Camera */
    private void openCamera() {

        /** Get Camera Service */
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            assert manager != null;

            /** Get main cam id */
            cameraId = manager.getCameraIdList()[0];

            /** Get Camera Characteristics */
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            assert map != null;

            /** Get image dimension (the biggest size) */
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];

            /** change TextureView to fit Camera's aspect ratio */
            int cameraWidth = imageDimension.getWidth();
            int cameraHeight = imageDimension.getHeight();
            float cameraAspectRatio;

            if (cameraWidth <= cameraHeight) {
                cameraAspectRatio = (float) cameraHeight / cameraWidth;
            } else {
                cameraAspectRatio = (float) cameraWidth / cameraHeight;
            }

            int fitWidth;
            int fitHeight;
            fitWidth = mTexturePreview.getWidth();
            fitHeight = (int) (mTexturePreview.getWidth() * cameraAspectRatio);
            mTexturePreview.setLayoutParams(new RelativeLayout.LayoutParams(fitWidth, fitHeight));

            /** Check realtime permission if run higher API 23 */
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, REQUEST_CAMERA_PERMISSION);
                return;
            }

            /** Open camera */
            manager.openCamera(cameraId, stateCallback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "You can't use camera without permission", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    TextureView.SurfaceTextureListener textureListen = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        startPredictThread();
        if (mTexturePreview.isAvailable())
            openCamera();
        else
            mTexturePreview.setSurfaceTextureListener(textureListen);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void startPredictThread() {
        mPredictThread = new HandlerThread("Predict Background");
        mPredictThread.start();
        mPredictHandler = new Handler(mPredictThread.getLooper());
        mPredictHandler.post(mDoPredict);
    }

    @Override
    protected void onPause() {
        stopBackgroundThread();
        stopPredictThread();
        super.onPause();
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void stopPredictThread() {
        mPredictThread.interrupt();
        mPredictThread.quitSafely();
    }
}
